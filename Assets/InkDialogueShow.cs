﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.UIElements;
using UnityEngine.UI;

public class InkDialogueShow : MonoBehaviour
{
    public TextAsset inkJSON;
    private Story story;
    private string text;
    public dialCanvasScript dialCanvas;
    private bool colliding;

    // Start is called before the first frame update
    void Start()
    {
        story = new Story(inkJSON.text);
        //dialCanvas = GameObject.FindGameObjectWithTag("UI").GetComponent<dialCanvasScript>();
        dialCanvas = GameObject.Find("Canvas").GetComponent<dialCanvasScript>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (colliding)
            loadStoryChunk();

            //Debug.Log(text);
        }
    }

    string loadStoryChunk()
    {
        string text = "";
        if (story.canContinue)
        {
            dialCanvas.dialText.gameObject.SetActive(true);
            dialCanvas.dialBox.gameObject.SetActive(true);
            dialCanvas.dialText.text = story.Continue();
            // TODO CHECK OVERFLOW
        }
        else
        {
            dialCanvas.dialText.gameObject.SetActive(false);
            dialCanvas.dialBox.gameObject.SetActive(false);
            dialCanvas.dialText.text = "";
        }

        return text;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            colliding = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            colliding = false;
        }
    }
}
