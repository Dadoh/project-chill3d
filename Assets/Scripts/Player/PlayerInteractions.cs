﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteractions : MonoBehaviour
{
    public bool collidingDirt;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (collidingDirt)
                Debug.Log("collision w dirt");
        }

        Debug.Log(collidingDirt);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Dirt")
        {
            collidingDirt = true;
        }

        
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Dirt")
        {
            collidingDirt = false;
        }

    }

}
