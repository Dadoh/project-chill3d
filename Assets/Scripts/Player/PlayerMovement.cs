﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float speed;
    public float forceConst;
    private Rigidbody rb;
    private bool isGrounded;
    private ConstantForce gravity;
    private Vector3 initGravity = new Vector3(0.0f, -9.81f, 0.0f);
    private Vector3 glideGravity = new Vector3(0.0f, -0.5f, 0.0f);

    public Vector3 playerPos;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        gravity = gameObject.AddComponent<ConstantForce>();
        gravity.force = initGravity;
        rb.useGravity = false;
    }


    void Update()
    {
        Debug.Log(gravity.force);

        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.forward * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.back * speed * Time.deltaTime;
        }

        if (isGrounded)
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                rb.AddForce(0, forceConst, 0, ForceMode.Impulse);
            }
        }

        //planare
        if (rb.velocity.y <= -10)
        {
            Debug.Log("sto cadendo");
            gravity.force = glideGravity;

        }

        playerPos = new Vector3(this.transform.position.x, this.transform.position.y);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
            gravity.force = initGravity;
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = false;
        }
    }
}