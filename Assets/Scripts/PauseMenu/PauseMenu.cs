﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PauseMenu : MonoBehaviour
{
    public bool isActive;
    [SerializeField]
    private GameObject pausePanel;


    // Start is called before the first frame update
    void Start()
    {
        isActive = false;
        pausePanel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) //TEMP  KEYS
        {
            if (!isActive)
                isActive = true;
            else
            {
                isActive = false;
            }
        }

        checkActive();

    }

    void checkActive()
    {
        if (isActive)
        {
            if (pausePanel)
            {
                pausePanel.SetActive(true);
                Time.timeScale = 0; //TEMP PAUSE
            }
            else
            {
                Debug.LogError("Missing Pause Canvas reference");
            }
        }
        else
        {
            if (pausePanel)
            {
                pausePanel.SetActive(false);
                Time.timeScale = 1; //TEMP RESUME
            }
            else
            {
                Debug.LogError("Missing Pause Canvas reference");
            }
        }
    }
}
