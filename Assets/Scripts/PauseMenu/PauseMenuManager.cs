﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PauseMenuManager : MonoBehaviour
{
    private UnityEvent backEvent;
    [SerializeField]
    private PauseMenu pauseMenu;

    // Start is called before the first frame update
    void Start()
    {
        pauseMenu = GameObject.Find("GameController").GetComponent<PauseMenu>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Use()
    {
        if (pauseMenu)
        {
            pauseMenu.isActive = false;
            Debug.Log("using button back");
        }
        else
        {
            Debug.LogError("Missing pauseMenu");
        }
    }
}
