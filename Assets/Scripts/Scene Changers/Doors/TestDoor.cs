﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestDoor : MonoBehaviour
{
    [Header("Scene Name \n")]
    [SerializeField]
    private string[] scenePath;
    

    private void Start()
    {
        //thisHouseScene = SceneManager.GetSceneByName("SampleHouseScene"); //TODO replace with serializefield and seet house in the editor to generalize

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player with door");
            if (scenePath[0] != "")
            {
                SceneManager.LoadScene(scenePath[0]);
            }
            else
            {
                Debug.LogError("Missing scene to load");
            }
        }
    }
}