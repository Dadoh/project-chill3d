﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TimeManagerScript : MonoBehaviour
{

    public float second, minute, hour, day, month, year;
    public float timeScale = 45f;
    public string season;
    [SerializeField]
    private clockCanvasScript clockText;
    

    // Start is called before the first frame update
    void Start()
    {
        clockText = GameObject.FindGameObjectWithTag("Canvas UI").GetComponent<clockCanvasScript>();

    }

    // Update is called once per frame
    void Update()
    {
        CalculateTime();
        //Debug.Log("Giorno " + day + " " + hour + ":" + minute + ":" + second);
        if (clockText != null)
        {
            clockText.hourText.text = hour.ToString() + ":" + minute.ToString();
        }
        else
        {
            Debug.LogError("Missing clockText reference");
        }
    }

    void CalculateTime()
    {
        second += Time.deltaTime * timeScale;
        if (second >= 60)
        {
            minute++;
            second = 0;
        }
        else if (minute >= 60)
        {
            hour++;
            minute = 0;
        }
        else if (hour >= 24)
        {
            day++;
            hour = 0;
        }
        else if (day >= 30)
        {
            month++;
            day = 1;
        }
        else if (month > 12)
        {
            year++;
            month = 1;
        }

        switch (month) //SEASON CHANGE
        {
            case 1:
            case 2:
            case 3:
                season = "Winter";
                break;

            case 4:
            case 5:
            case 6:
                season = "Spring";
                break;

            case 7:
            case 8:
            case 9:
                season = "Summer";
                break;

            case 10:
            case 11:
            case 12:
                season = "Fall";
                break;
        }

        if (month <= 0) //RESET
        {
            month = 1;
        }
        if (day <= 0)
        {
            day = 1;
        }
        if (year <= 0)
        {
            year = 1;
        }

    }
}
