﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform playerTransform;
    private Vector3 cameraOffset;

    public float speedH;
    public float speedV;

    private float horRot = 0.0f;
    private float verRot = 0.0f;

    [Range(0.01f, 1.0f)]
    public float smoothnessFloat;

    void Start()
    {
        cameraOffset = transform.position - playerTransform.position;
    }

    void LateUpdate()
    {
        //player follow
        Vector3 newPos = playerTransform.position + cameraOffset;
        transform.position = Vector3.Slerp(transform.position, newPos, smoothnessFloat);

        //mouse movement
        horRot += speedH * Input.GetAxis("Mouse X");
        verRot -= speedV * Input.GetAxis("Mouse Y");

        //camera rotation limit
        horRot = Mathf.Clamp(horRot, -20f, 20f);
        verRot = Mathf.Clamp(verRot, 15f, 30f);


        transform.eulerAngles = new Vector3(verRot, horRot, 0.0f);
    }
}
