﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIWander : MonoBehaviour
{
    public float speed;
    public Transform[] wanderSpots;
    private int wanderRnd;
    private float idleTime;
    public float startIdleTime;
    void Start()
    {
        wanderRnd = Random.Range(0, wanderSpots.Length);
        idleTime = startIdleTime;
    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, wanderSpots[wanderRnd].position, speed *  Time.deltaTime);
        

        if (Vector3.Distance(transform.position, wanderSpots[wanderRnd].position) < 0.5f)
        {
            if (idleTime <= 0)
            {
                wanderRnd = Random.Range(0, wanderSpots.Length);
                idleTime = startIdleTime;
            }
            else
            {
                idleTime -= Time.deltaTime;
            }
        }
    }
}
