﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryMenuShow : MonoBehaviour
{
    [SerializeField]
    private GameObject inventoryCanvas;
    private bool inventoryOn;

    // Start is called before the first frame update
    void Start()
    {
        if (inventoryCanvas)
        {
            inventoryCanvas.SetActive(false);
            inventoryOn = false;
        }
        else
        {
            Debug.LogError("No inventory canvas found");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (inventoryCanvas)
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                if (!inventoryOn)
                {
                    inventoryCanvas.SetActive(true);
                    inventoryOn = true;
                }
                else if (inventoryOn)
                {
                    inventoryCanvas.SetActive(false);
                    inventoryOn = false;
                }
            }
        }
        else
        {
            Debug.LogError("No inventory canvas found");
        }
    }
}
