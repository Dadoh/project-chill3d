﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    [Header("UI Parameters")]
    [SerializeField]
    private TextMeshProUGUI itemNumberText;
    [SerializeField]
    private Image itemImage;

    [Header("Item Variables")]
    public InventoryItem inventoryItem;
    public InventoryManager inventoryManager;

    public void Setup(InventoryItem newItem, InventoryManager newManager)
    {
        inventoryItem = newItem;
        inventoryManager = newManager;

        if (inventoryItem)
        {
            itemImage.sprite = inventoryItem.icon;
            itemNumberText.text = inventoryItem.numberHeld.ToString();
        }
    }

    public void ClickedOn()
    {
        if (inventoryItem)
        {
            inventoryManager.SetupDescription(inventoryItem.itemDescription, inventoryItem.isUsable, inventoryItem);
        }
    }
}
