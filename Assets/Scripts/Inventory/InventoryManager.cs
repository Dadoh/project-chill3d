﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventoryManager : MonoBehaviour
{
    [Header("Inventory Information")]
    public PlayerInventory playerInventory;
    [SerializeField]
    private GameObject blankSlot;
    [SerializeField]
    private GameObject inventoryPanel;
    [SerializeField]
    private TextMeshProUGUI descriptionText;
    [SerializeField]
    private GameObject useButton;
    public InventoryItem currentItem;

    void CreateSlots()
    {
        if (playerInventory)
        {
            for (int i = 0; i < playerInventory.inventoryList.Count; i++)
            {
                if (playerInventory.inventoryList[i].numberHeld > 0)
                {
                    GameObject temp = Instantiate(blankSlot, inventoryPanel.transform.position, Quaternion.identity);
                    temp.transform.SetParent(inventoryPanel.transform);
                    InventorySlot newSlot = temp.GetComponent<InventorySlot>();

                    if (newSlot)
                    {
                        newSlot.Setup(playerInventory.inventoryList[i], this);
                    }
                }
            }
        }
    }

    void OnEnable()
    {
        ClearSlots();
        CreateSlots();
        SetTextButton("", false);
    }

    public void SetTextButton(string description, bool isButtonActive)
    {
        descriptionText.text = description;

        if (isButtonActive)
        {
            useButton.SetActive(true);
        }

        else
        {
            useButton.SetActive(false);
        }
    }

    public void SetupDescription(string newDescription, bool isUsable, InventoryItem newItem)
    {
        currentItem = newItem;
        descriptionText.text = newDescription;
        useButton.SetActive(isUsable);
    }

    void ClearSlots()
    {
        for (int i = 0; i < inventoryPanel.transform.childCount; i++)
        {
            Destroy(inventoryPanel.transform.GetChild(i).gameObject);
        }
    }

    public void UseButtonPressed()
    {
        if (currentItem)
        {
            currentItem.Use();
            //clear 0 slots
            ClearSlots();
            //reefill slots with new numbers
            CreateSlots();
            if (currentItem.numberHeld == 0)
            SetTextButton("", false); //deletes description and button
        }
    }
}
