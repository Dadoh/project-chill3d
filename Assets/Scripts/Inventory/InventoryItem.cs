﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory/Items")]
public class InventoryItem : ScriptableObject
{
    public string itemName;
    public string itemDescription;
    public int numberHeld;
    public Sprite icon;
    public bool isUsable;
    public bool isUnique;
    public UnityEvent useEvent;

    public void Use()
    {
        //Debug.Log("Item used");
        if (numberHeld > 0)
        useEvent.Invoke();
    }

    public void DecreaseItem(int amountToDecrease)
    {
        numberHeld -= amountToDecrease;
        if (numberHeld < 0)
        {
            numberHeld = 0;
        }
    }
}
