﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableInventoryItem : MonoBehaviour
{
    [SerializeField]
    private PlayerInventory playerInventory;
    [SerializeField]
    private InventoryItem thisItem;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("collision player with collectable");
            AddItem();
            Destroy(this.gameObject); //if you dont want them to respawn
        }
    }

    void AddItem()
    {
        if (playerInventory && thisItem)
        {
            if (playerInventory.inventoryList.Contains(thisItem))
            {
                thisItem.numberHeld++;
            }
            else
            {
                playerInventory.inventoryList.Add(thisItem);
                thisItem.numberHeld++;
            }
        }
    }
}
