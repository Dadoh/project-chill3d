﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SeedScript : MonoBehaviour
{
    public InventoryItem inventoryItem;
    public Transform player;
    public PlayerInteractions playerInteractions;
    private Vector3 playerPos;
    [SerializeField]
    private GameObject thisPrefab;

    [Header("SEED VARIABLES \n")]
    public float lifeSpan;
    public string[] seasonsToGrow;
    public bool isReady;
    public bool isPlanted;

    private bool playerDirt;

    private void Start()
    {
        player = GameObject.Find("Player").GetComponent<Transform>();
        playerInteractions = GameObject.Find("Player").GetComponent<PlayerInteractions>();
        playerPos = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z);
        this.transform.position = playerPos;

        isReady = false;
        isPlanted = true;
        playerDirt = playerInteractions.collidingDirt;

    }

    private void Update()
    {
        seedGrowing();
    }

    public void Use()
    {
        if (inventoryItem && player)
        {

                if (inventoryItem.numberHeld > 0)
                {
                    if (playerInteractions)
                    {
                        if (playerDirt)
                        {
                            Debug.Log("usato il seme");
                            Instantiate(thisPrefab, playerPos, Quaternion.identity);
                            Debug.Log(player.transform.position);
                        }
                    }
                    else
                    {
                        Debug.LogError("Player Interactions not found!");
                    }
                }           
        }
        else
        {
            Debug.LogError("Inventory Item or Player not found!");
        }
    }

    private void seedGrowing()
    {
        if (isPlanted)
        {
            lifeSpan -= Time.deltaTime;
        }
        if (lifeSpan <= 0)
        {
            lifeSpan = 0;
            isReady = true;
        }
    }
}
