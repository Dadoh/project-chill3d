﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dialCanvasScript : MonoBehaviour
{
    public Text dialText;
    public Image dialBox;
    void Start()
    {
        //dialText.enabled = false;
        dialText.gameObject.SetActive(false);
        dialBox.gameObject.SetActive(false);

    }

}